from api_library import DroneController
import time

droneHandle = DroneController('cf729b1bae289305d802b5d01bf58b1b55c1cb87', 'd2XMbwMe')

print "demo"

print "takeoff"
print droneHandle.take_off(2.0)

print "going towards side 1:"," ","5 metres"
droneHandle.position_set_local(5,0,0,0,0.3,False,False,False,True)

print "going towards side 2:"," ","5 metres"
droneHandle.position_set_local(0,5,0,0,0.3,False,False,False,True)

print "going towards side 3:"," ","5 metres"
droneHandle.position_set_local(-5,0,0,0.3,0,False,False,False,True)

print "going towards side 4:"," ","5 metres"
droneHandle.position_set_local(0,-5,0.0,0.3,0,False,False,False,True)

print "land"
print droneHandle.land(True)

